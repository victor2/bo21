@echo off
set DEST=..\..\..\..\BuddhaOrc
if exist %DEST%\nul goto already
mkdir %DEST%
mkdir %DEST%\data
mkdir %DEST%\data\GUI
:already
copy bin\*.dll %DEST%
copy bin\*.exe %DEST%
rem copy ..\boc\bin\boc.exe %DEST%
rem copy ..\boc\bin\default.ini %DEST%\bconfig.ini
copy bin\default.ini %DEST%\config.ini
copy bin\meanquar.scl %DEST%
copy bin\data\image1.jpg %DEST%\data
copy bin\data\*.ttf %DEST%\data
copy bin\data\GUI\*.* %DEST%\data\GUI

exit
pushd
cd %DEST%

popd
