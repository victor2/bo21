#include "AllThreads.h"

std::list<ofThread*> AllThreads::threads;
std::list<ofThread*> AllThreads::stopped;
std::mutex AllThreads::mutx;

void AllThreads::addThread(ofThread* t)
{
    mutx.lock();
    threads.push_back(t);
    mutx.unlock();
}

void AllThreads::removeThread(ofThread* t)
{
    mutx.lock();
    threads.remove(t);
    stopped.push_back(t);
    mutx.unlock();
}

int AllThreads::getCount()
{
    mutx.lock();
    int n = threads.size();
    mutx.unlock();
    return n;
}

void AllThreads::stopAll()
{
    mutx.lock();
	for (auto t : threads)
	{
        t->stopThread();
    }
    mutx.unlock();
}

void AllThreads::cleanUp()
{
    mutx.lock();
	for (auto t : threads)
	{
     //   delete t;
    }
    stopped.clear();
    mutx.unlock();
}

void AllThreads::dump()
{
    mutx.lock();
	for (auto t : threads)
	{
        std::cout << t->getThreadName() << " ";
    }
    mutx.unlock();
}
