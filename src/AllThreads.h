#ifndef ALLTHREADS_H
#define ALLTHREADS_H

#include <list>
#include <mutex>
#include "ofThread.h"

class AllThreads
{
    public:
        static void addThread(ofThread* t);
        static void removeThread(ofThread* t);
        static int getCount();
        static void stopAll();
        static void cleanUp();
        static void dump();

    private:
        static std::list<ofThread*> threads;
        static std::list<ofThread*> stopped;
        static std::mutex mutx;
};

#endif // ALLTHREADS_H
