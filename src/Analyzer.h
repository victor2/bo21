#ifndef ANALYZER_H
#define ANALYZER_H

#include <list>
#include <vector>

#include "ofxOpenCv.h"
#include "Melody.h"

// per plane
#define MAX_LOOPS 10
#define NUM_PLANES 4
#define PIX(px, x, y, w) (px)[(x) + (y) * (w)]

//class ofxIniSettings;

class NoteSplitter
{
    std::list<int> notes;
    float weight;
public:
    NoteSplitter(string s);
    std::list<int> &getSplit() { return notes; }
    float getWeight() { return weight; }
};

class Analyzer
{
        int wd, ht;
        int minShapeWd;
        int minShapeHt;
        static std::vector<NoteSplitter*> splitters;
    public:
        Analyzer();
        virtual ~Analyzer();
        void init(ofxIniSettings &ini);
        int analyze(ofImage &img);
        void generate(NoteDuration *nd);
        Melody * addMelody(int blobId, int id, int trans, ofColor cols);
        void addPauses();
        std::list<Melody*> m_Music;
        static std::list<int> &getNoteSplit();
        int m_NextId;

        static int transposes[MAX_GROUP_SZ];
        static int lowest, highest, maxnote, maxpause;
        static ofxCvContourFinder cf;
};

#endif // ANALYZER_H
