#include "Chord.h"

int CHord::numMatrices = 0;
int CHord::currentMatrix = 0;
std::vector<CHord*> CHord::matrices;

#include "ofMain.h"
#include "ofxIniSettings.h"


CHord::CHord()
{
    //ctor
}

CHord::~CHord()
{
    //dtor
}

std::list<int> &CHord::getChord(int n)
{
    return matrices[currentMatrix]->chords[n];
}

void CHord::init(ofxIniSettings &ini)
{
    for (numMatrices = 0; ; numMatrices++)
    {
        CHord *chrd = new CHord();
        bool notdefined = false;
        for (int i=0; i<12; ++i)
        {
            string suffix = ofToString(i) + "#" + ofToString(numMatrices);
            string chord = ini.get(string("chord") + suffix, string(""));
            if (!numMatrices && (chord.length() < 12))
            {
                chord = ini.get(string("chord") + ofToString(i), string(""));
                notdefined = true;
            }
            if (chord.length() < 12)
            {
                chrd->chords[i].push_back(i);
                notdefined = true;
            }
            else
            {
                for (int j=0; j<12; j++)
                {
                    if (chord[j] != '0' && chord[j] != '.') chrd->chords[i].push_back(j);
                }
                cout << "i=" << i << " " << chord << endl;
            }
        }

        if (notdefined)
        {
            if (numMatrices)
            {
                delete chrd;
            }
            else
            {
                matrices.push_back(chrd);
            }
            break;
        }

        matrices.push_back(chrd);
    }
}

string CHord::getRange()
{
    if (numMatrices < 2) return "";
    char buf[20];
    sprintf(buf, "a..%c", 'a' + numMatrices - 1);
    return string(buf);
}

string CHord::getInfo(int n)
{
    char mx[2] = {'a', 0};
    mx[0] += n < 0 ? currentMatrix : n;
    return string(mx);
}

bool CHord::keypressed(int key)
{
    int m = key - 'a';
    if (m >=0 && m < numMatrices)
    {
        cout << "matrix " << m << endl;
        currentMatrix = m;
        return true;
    }
    return false;
}
