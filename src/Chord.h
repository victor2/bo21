#ifndef CHORD_H
#define CHORD_H

#include <list>
#include <vector>
#include <string>
class ofxIniSettings;

class CHord
{
    public:
        CHord();
        virtual ~CHord();
        static void init(ofxIniSettings &ini);
        static std::list<int> &getChord(int n);
        static std::string getRange();
        static std::string getInfo(int n = -1);
        static int getNumber() { return numMatrices; }
        static bool keypressed(int key);
    protected:
    private:
        static int numMatrices;
        static int currentMatrix;
        std::list<int> chords[12];
        static std::vector<CHord*> matrices;
};

#endif // CHORD_H
