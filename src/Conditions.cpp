#include "Conditions.h"

std::map<int, CondRuleList*> Conditions::rules;

Conditions::Conditions()
{
    //ctor
}

Conditions::~Conditions()
{
    //dtor
}

CondRuleBase::CondRuleBase()
{
    //ctor
    m_Active = false;
}

CondRuleBase::~CondRuleBase()
{
    //dtor
}

void Conditions::init(ofxIniSettings &ini)
{
    for(int i = 0; i < MAX_CONDITIONS; ++i)
    {
        string key = string("rule" + ofToString(i));
        string val = ini.get(key, string(""));
        if (val != "")
        {
            vector<string> cmds = ofSplitString(val, ";", true, true);
            if (cmds.size() == 0) continue;

            CondRuleList* rlist;
            map<int, CondRuleList*>::iterator it = rules.find(i);
            if (it != rules.end())
            {
                rlist = it->second;
            }
            else
            {
               rlist = rules[i] = new CondRuleList();
            }

            for (int cmdIdx = 0; cmdIdx < cmds.size(); cmdIdx++)
            {
                char tp[100];
                int v1, v2 = -1;
                int cnt = sscanf(cmds[cmdIdx].c_str(), "%s %i %i", tp, &v1, &v2);
                if (cnt >= 2 && strcmp(tp, "midi") == 0)
                {
                    rlist->add( new CondMidiRule(v1, v2) );
                }
                else if (cnt >= 1 && strcmp(tp, "serial") == 0)
                {
                    rlist->add( new CondSerialRule(v1) );
                }
                else
                {
    cout << "ignored '" << cmds[cmdIdx] << "' cnt=" << cnt << " tp='" << tp << "'" << endl;
                }
            }
        }
    }
}

bool Conditions::handle(ofxMidiMessage& e)
{
    bool handled = false;
    for (map<int, CondRuleList*>::iterator it = rules.begin(); it != rules.end(); it++)
    {
        CondRuleList *r = it->second;
        if (r->handle(e)) handled = true;
    }
    if (e.control == 41 && e.value != 0) dump();
    return handled;
}

bool Conditions::handle(int b) // a byte from serial port
{
    bool handled = false;
    for (map<int, CondRuleList*>::iterator it = rules.begin(); it != rules.end(); it++)
    {
        CondRuleList *r = it->second;
        if (r->handle(b)) handled = true;
    }
    return handled;
}

void Conditions::dump()
{
    for (map<int, CondRuleList*>::iterator it = rules.begin(); it != rules.end(); it++)
    {
        cout << it->first << endl;
        it->second->dump();
    }
}

bool Conditions::isActive(int melId)
{
    map<int, CondRuleList*>::iterator it = rules.find(melId);
    if (it == rules.end())
    {
        return false;
    }
    else
    {
        CondRuleList *r = it->second;
        return r->isActive();
    }
}

bool CondMidiRule::handle(ofxMidiMessage& e)
{
    if (m_Off > 0)
    {
        if (e.control == m_Off) { m_Active = false; return true; }
        if (e.control == m_On) { m_Active = true; return true; }
    }
    else
    {
        if (e.control == m_On) { m_Active = e.value != 0; return true; }
    }
    return false;
}

bool CondRuleList::handle(ofxMidiMessage& e)
{
    bool handled = false;
    for (list<CondRuleBase*>::iterator it = m_rules.begin(); it != m_rules.end(); it++)
    {
        CondRuleBase* r = *it;
        if (r->getType() == RULE_MIDI)
        {
            CondMidiRule *mr = dynamic_cast<CondMidiRule*>(r);
            if (mr->handle(e)) handled = true;
        }
    }
    return handled;
}

bool CondRuleList::handle(int b)
{
    bool handled = false;
    for (list<CondRuleBase*>::iterator it = m_rules.begin(); it != m_rules.end(); it++)
    {
        CondRuleBase* r = *it;
        if (r->getType() == RULE_SERIAL)
        {
            CondSerialRule *mr = dynamic_cast<CondSerialRule*>(r);
            if (mr->handle(b)) handled = true;
        }
    }
    cout << b << " ";
    return handled;
}

bool CondRuleList::isActive()
{
    for (list<CondRuleBase*>::iterator it = m_rules.begin(); it != m_rules.end(); it++)
    {
        CondRuleBase* r = *it;
        if (r->check()) return true;
    }
    return false;
}

bool CondSerialRule::handle(int b)
{
    m_Active = (b & m_Bit) != 0;
    return true;
}
