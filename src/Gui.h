#ifndef GUI_H
#define GUI_H

#include "ofxIniSettings.h"
#include "ofxGui.h"
#include "Envelope.h"

class ofApp;
class Replicator;
class Melody;

class Gui
{
    public:
        Gui();
        virtual ~Gui();

        void init(ofApp *a, ofxIniSettings &ini);
        void showImageDir();
        void showMeloDir();
        void showGear(bool show) { visible = show; }

        void guiEvent(ofEventArgs &e);
        void guiDirEvent(ofEventArgs &e);
        void updateGui(bool pl = true);
        void updatePlaying();
        void updateInstruments(Melody *me);

        bool isHit(float x, float y) {
			return false; 
			/*guiM->isHit(x, y) ||
            (guiI && guiI->isHit(x, y)) ||
            (guiP && guiP->isHit(x, y)) ||
            guiS->isHit(x, y) ||
            guiD->isHit(x, y) ||
            guiG->isHit(x, y) ||
            (guiLoadImg && guiLoadImg->isHit(x, y)) ||
            (guiLoadMel && guiLoadMel->isHit(x, y)); */
        }

        bool isVisible() {
            return visible;
        }
        void toggleVisible();

		void draw();

        // envelop sliders are public so they can be adjusted by MIDI events
		ofxSlider<int> envelSliders[NSLIDERS];

    protected:
    private:
		bool visible = true;
        // a little icon in the corner
		ofxPanel gear;
        ofxToggle gearBtn;

		ofxPanel guiM, guiS, guiI, guiD, guiG, guiP, guiLoadImg, guiLoadMel;
        ofxToggle play;
        ofxToggle envelToggle;
        ofxToggle veloToggle;
        ofxToggle noteToggle;
        ofxToggle trackToggle;
        ofxToggle reverseToggle;
        ofxToggle muteToggle;
        ofxToggle soloToggle;
        ofxButton reduceBtn;
        ofxButton extremeBtn;
        ofxButton mergeBtn;
        ofxButton splitBtn;
        ofxButton pauseBtn;
        ofxButton rmPauseBtn;
        ofxButton restoreBtn;
        ofxButton assignBtn;

        // Sliders
		ofxSlider<int> drumSlider;
		ofxSlider<int> echoSlider;
		ofxSlider<int> volumeSlider;
		ofxSlider<float> dynamSlider;
		ofxSlider<float> stacattoSlider;
		ofxSlider<int> qualitySlider;

		/*
        ofxRadio *destList;
        ofxRadio *instrList;
        ofxRadio *chordList;
        ofxRadio *chordTypeList;
        ofxRadio *drumList;
		*/

        // global
        ofxToggle pause;
		ofxSlider<int> tickSlider;
		ofxSlider<int> bpmSlider;
		ofxSlider<float> alphaSlider;
		ofxSlider<int> arpeggioSlider;
        //ofxRadio *midiList;
        //ofxRadio *playingList;
        //ofxRadio *drummingList;
        ofxButton boConfigBtn;
        ofxButton imageDirBtn;
        ofxButton meloDirBtn;
        ofxButton quitBtn;
        ofxToggle windowToggle;

        // directory lists
//        ofxRadio *imageFiles;
//        ofxRadio *melodyFiles;

        ofApp *ta;
        int wd;

		bool dirty = false;
		bool update = false;

//        void activateRadioById(ofxRadio *r, int id);
//        ofxRadio *addFilesRadio(ofxPanel *gui, string dir, string caption, string ext, string filter = "");

		void playClicked(bool &clicked);
};

#endif // GUI_H
