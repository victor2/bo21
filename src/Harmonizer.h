#ifndef HARMONIZER_H
#define HARMONIZER_H

#include <string>
#include "ofxMidi.h"

class Melody;

// number of notes in one octave, including ##
#define NOTE_NUM 12

class Harmonizer
{
    public:
        Harmonizer();
        virtual ~Harmonizer();
        static void init(ofxIniSettings &ini);
        static void clearHistogram() { memset(&m_noteHist, 0, sizeof(m_noteHist)); }
        static void addHistogram(Melody *mel);
        static void processHistogram();
        static void dumpHistogram();
        // uses either interval or not probabilities
        static void processMelody(Melody *mel, bool intrv);

        // note - the initial note taken from position on image
        // prev - previous note is needed to harmonize intervals
        // relTime - 0..1 - current position within a measure
        static int harmonize(int note, int prev, float relTime);

        enum ChordType { MINOR, MAJOR, AUGMENTED, DIMINISHED, CUSTOM };

        static ChordType m_ctype;
    protected:
    private:
        // the melody histogram
        static int m_noteHist[NOTE_NUM];
        // computed from the histogram
        static int m_harmBase;
        // probabilities for notes
        static int m_noteProbs[NOTE_NUM];
        // probabilities for intervals
        static int m_intrProbs[NOTE_NUM];
};

#endif // HARMONIZER_H
