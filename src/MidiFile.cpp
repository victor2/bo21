#include "MidiFile.h"
#include "ofxMidiConstants.h"
#include <fstream>

// ported to C++ from http://kevinboone.net/javamidi.html

const unsigned char MidiFile::header[] = {
     0x4d, 0x54, 0x68, 0x64, 0x00, 0x00, 0x00, 0x06,
     0x00, 0x00, // single-track format
     0x00, 0x01, // one track
     0x00, 0x10, // 16 ticks per quarter
     0x4d, 0x54, 0x72, 0x6B
     };

 unsigned char MidiFile::footer[] = {
     0x01, 0xFF, 0x2F, 0x00
     };

 unsigned char MidiFile::tempoEvent[] = {
     0x00, 0xFF, 0x51, 0x03,
     0x0F, 0x42, 0x40 // Default 1 million usec per crotchet
     };

 unsigned char MidiFile::keySigEvent[] = {
     0x00, 0xFF, 0x59, 0x02,
     0x00, // C
     0x00  // major
     };

 unsigned char MidiFile::timeSigEvent[] = {
     0x00, 0xFF, 0x58, 0x04,
     0x04, // numerator
     0x02, // denominator (2==4, because it's a power of 2)
     0x30, // ticks per click (not used)
     0x08  // 32nd notes per crotchet
     };


MidiFile::MidiFile()
{
    //ctor
}

MidiFile::~MidiFile()
{
    //dtor
    for (int i = 0; i < playEvents.size(); i++)
    {
      MidiEvent * me = playEvents[i];
      delete me;
    }

}

/** Write the stored MIDI events to a file */
void MidiFile::writeToFile (string filename) {
    ofstream fos(filename.c_str(), ios_base::out|ios_base::binary);

    fos.write((const char*)header, sizeof header);

    // Calculate the amount of track data
    // _Do_ include the footer but _do not_ include the
    // track header

    int size = sizeof tempoEvent + sizeof keySigEvent + sizeof timeSigEvent
      + sizeof footer;

    for (int i = 0; i < playEvents.size(); i++)
      size += playEvents[i]->len;

    // Write out the track data size in big-endian format
    // Note that this math is only valid for up to 64k of data
    //  (but that's a lot of notes)
    char zero = 0;
    unsigned char high = size / 256;
    unsigned char low = size & 0xff;
    fos.write (&zero, 1);
    fos.write (&zero, 1);
    fos.write ((char *)&high, 1);
    fos.write ((char *)&low, 1);


    // Write the standard metadata � tempo, etc
    // At present, tempo is stuck at crotchet=60
    fos.write ((char*)tempoEvent, sizeof tempoEvent);
    fos.write ((char*)keySigEvent, sizeof keySigEvent);
    fos.write ((char*)timeSigEvent, sizeof timeSigEvent);

    // Write out the note, etc., events
    for (int i = 0; i < playEvents.size(); i++)
    {
      MidiEvent * me = playEvents[i];
      fos.write ((char*) me->d, me->len);
    }

    // Write the footer and close
    fos.write ((char*)footer, sizeof footer);
    fos.close();
  }

  /** Store a note-on event */
  void MidiFile::noteOn (int delta, int note, int velocity)
  {
  MidiEvent *me = new MidiEvent(4);
  me->d[0] = delta;
  me->d[1] = MIDI_NOTE_ON;
  me->d[2] = note;
  me->d[3] = velocity;
  playEvents.push_back (me);
  }


  /** Store a note-off event */
  void MidiFile::noteOff (int delta, int note)
  {
  MidiEvent *me = new MidiEvent(4);
  me->d[0] = delta;
  me->d[1] = MIDI_NOTE_OFF;
  me->d[2] = note;
  me->d[3] = 0;
  playEvents.push_back (me);
  }

  /** Store a program-change event at current position */
  void MidiFile::progChange (int prog)
  {
  MidiEvent *me = new MidiEvent(3);
  me->d[0] = 0;
  me->d[1] = MIDI_PROGRAM_CHANGE;
  me->d[2] = prog;
  playEvents.push_back (me);
  }

  /** Store a CC event */
  void MidiFile::controlChange (int b1, int b2)
  {
  MidiEvent *me = new MidiEvent(4);
  me->d[0] = 0;
  me->d[1] = MIDI_CONTROL_CHANGE;
  me->d[2] = b1;
  me->d[3] = b2;
  playEvents.push_back (me);
  }

  /** Store a pitch bend event */
  void MidiFile::pitchBend(int value)
  {
  MidiEvent *me = new MidiEvent(4);
  me->d[0] = 0;
  me->d[1] = MIDI_PITCH_BEND;
  me->d[2] = value & 0x7F;
  me->d[3] = (value >> 7) & 0x7F;

  playEvents.push_back (me);
  }


//  /** Test method � creates a file test1.mid when the class
//      is executed */
//  public static void main (String[] args)
//    throws Exception
//  {
//    MidiFile mf = new MidiFile();
//
//    // Test 1 � play a C major chord
//
//    // Turn on all three notes at start-of-track (delta=0)
//    mf.noteOn (0, 60, 127);
//    mf.noteOn (0, 64, 127);
//    mf.noteOn (0, 67, 127);
//
//    // Turn off all three notes after one minim.
//    // NOTE delta value is cumulative � only _one_ of
//    //  these note-offs has a non-zero delta. The second and
//    //  third events are relative to the first
//    mf.noteOff (MINIM, 60);
//    mf.noteOff (0, 64);
//    mf.noteOff (0, 67);
//
//    // Test 2 � play a scale using noteOnOffNow
//    //  We don't need any delta values here, so long as one
//    //  note comes straight after the previous one
//
//    mf.noteOnOffNow (QUAVER, 60, 127);
//    mf.noteOnOffNow (QUAVER, 62, 127);
//    mf.noteOnOffNow (QUAVER, 64, 127);
//    mf.noteOnOffNow (QUAVER, 65, 127);
//    mf.noteOnOffNow (QUAVER, 67, 127);
//    mf.noteOnOffNow (QUAVER, 69, 127);
//    mf.noteOnOffNow (QUAVER, 71, 127);
//    mf.noteOnOffNow (QUAVER, 72, 127);
//
//    // Test 3 � play a short tune using noteSequenceFixedVelocity
//    //  Note the rest inserted with a note value of -1
//
//    int[] sequence = new int[]
//      {
//      60, QUAVER + SEMIQUAVER,
//      65, SEMIQUAVER,
//      70, CROTCHET + QUAVER,
//      69, QUAVER,
//      65, QUAVER / 3,
//      62, QUAVER / 3,
//      67, QUAVER / 3,
//      72, MINIM + QUAVER,
//      -1, SEMIQUAVER,
//      72, SEMIQUAVER,
//      76, MINIM,
//      };
//
//    // What the heck � use a different instrument for a change
//    mf.progChange (10);
//
//    mf.noteSequenceFixedVelocity (sequence, 127);
//
//    mf.writeToFile ("test1.mid");
//  }
//}
