#ifndef MIDIFILE_H
#define MIDIFILE_H
#include <vector>
#include <string>

using namespace std;
struct MidiEvent
{
    unsigned char *d;
    int len;
    MidiEvent() : d(0), len(0) {}
    MidiEvent(int dim) : len(dim), d(new unsigned char[dim]) {}
    virtual ~MidiEvent() { delete [] d; }
};

class MidiFile
{
    public:
        MidiFile();
        virtual ~MidiFile();

        /** Write the stored MIDI events to a file */
        void writeToFile (string filename);
        void noteOn (int delta, int note, int velocity);
        /** Store a note-off event */
        void noteOff (int delta, int note);
        /** Store a program-change event at current position */
        void progChange (int prog);
        /** Store a CC event */
        void controlChange (int b1, int b2);
        /** Store a pitch bend event */
        void pitchBend (int value);

    protected:
    private:
        // The collection of events to play, in time order
        vector<MidiEvent*> playEvents;

      // Note lengths
  //  We are working with 32 ticks to the crotchet. So
  //  all the other note lengths can be derived from this
  //  basic figure. Note that the longest note we can
  //  represent with this code is one tick short of a
  //  two semibreves (i.e., 8 crotchets)

  static const int SEMIQUAVER = 4;
  static const int QUAVER = 8;
  static const int CROTCHET = 16;
  static const int MINIM = 32;
  static const int SEMIBREVE = 64;

  // Standard MIDI file header, for one-track file
  // 4D, 54... are just magic numbers to identify the
  //  headers
  // Note that because we're only writing one track, we
  //  can for simplicity combine the file and track headers
  static const unsigned char header[];

  // Standard footer
  static unsigned char footer[];

  // A MIDI event to set the tempo
  static unsigned char tempoEvent[];

  // A MIDI event to set the key signature. This is irrelent to
  //  playback, but necessary for editing applications
  static unsigned char keySigEvent[];


  // A MIDI event to set the time signature. This is irrelent to
  //  playback, but necessary for editing applications
  static unsigned char timeSigEvent[];
};

#endif // MIDIFILE_H
