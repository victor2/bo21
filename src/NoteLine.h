#ifndef NOTELINE_H
#define NOTELINE_H

class Melody;
class NoteLine
{
    public:
        NoteLine();
        virtual ~NoteLine();
        void drawMelody(Melody *m, int col, int plcol);
    protected:
    private:
  		ofTrueTypeFont	font;
        // The lineNo is integer for notes EGBDF and has a half for FACE
        void placeNote(int no, bool treble, float &lineNo, bool &sharp);
};

#endif // NOTELINE_H
