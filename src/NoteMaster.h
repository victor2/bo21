#ifndef NOTEMASTER_H
#define NOTEMASTER_H

#include "ofThread.h"
#include <list>

class NotePlayer;

class NoteMaster : public ofThread
{
    public:
        NoteMaster();
        virtual ~NoteMaster();

        void threadedFunction();
        void play(NotePlayer *p);
        void stopAll();
        int getCount();

    protected:
    private:
        std::list<NotePlayer*> notes;
};

#endif // NOTEMASTER_H
