#include "TabMenu.h"
#include "Controller.h"

TabMenu::TabMenu()
{
    //ctor
    m_mode = 0;
    m_ctl = 0;
}

TabMenu::~TabMenu()
{
    //dtor
}

void TabMenu::draw(int w, int h)
{
    const int W = 200;
//   	char reportString[255];

    if (!m_mode) return;
    int mask = (m_mode == 1) ? MPLAY | MINST : MDRUM;
    int cnt = 0;
    list<Melody*>::iterator it;
    for (it = m_music->begin(); it != m_music->end(); it++)
    {
        Melody *m = *it;
        if (m->getPlayingState() & mask) cnt++;
    }

    ofSetHexColor((m_mode == 1) ? 0xffffff : 0xffddaa);
    if (!cnt)
    {
        ofDrawRectangle(w - W, 0, W, 18);
        ofSetHexColor(0x000000);
        ofDrawBitmapString("No matching melodies", w - W + 2, 15);
        return;
    }

    ofDrawRectangle(w - W, 0, W, cnt * 15 + 3);
    cnt = 0;
    for (it = m_music->begin(); it != m_music->end(); it++)
    {
        Melody *m = *it;
        int st = m->getPlayingState();
        if ((st & mask) == 0) continue;
        const char *pre = " ";
        int col = 0x000033;
        if (m_ctl->getSoloMelody() == m)
        {
            col = 0xaa0000;
            pre = "*";
        }
        else if (m_ctl->isSelected(m))
        {
            col = 0x007700;
            pre = "+";
        }
        if ((st & MPLAY) == 0)
        {
            col = 0x777777;
        }

        ofSetHexColor(col);
        char buf[256];
        sprintf(buf, "%2i%s%s", m->m_Id, pre, (m_mode == 1) ? m->instrInfo() : m->drumInfo());
        int xx = w - W + 2;
        int yy = ++cnt * 15;
        ofDrawBitmapString(buf, xx + 10, yy);
        if ((m_mode == 1) && (st & MPLAY))
        {
            int v = (m->getVolume() + 127) / 16;
            ofDrawRectangle(xx, yy-v, 8, v);
        }
    }
}

void TabMenu::control(int key)
{
    if (key == OF_KEY_TAB) // tab
    {
        m_mode = ++m_mode % 3;
        return;
    }
    if (m_mode)
    {
        bool solo = m_ctl->getSoloMelody() != 0;
        // key translation
        if (!solo)
        {
            switch(key)
            {
            case OF_KEY_DOWN: // down
                m_ctl->nextPlaying((m_mode == 1) ? MPLAY | MINST : MDRUM);
                m_ctl->getCurrentMelody()->display = 10;
                return;
            case OF_KEY_UP: //up
                m_ctl->prevPlaying((m_mode == 1) ? MPLAY | MINST : MDRUM);
                m_ctl->getCurrentMelody()->display = 10;
                return;
            }
        }
    }
    m_ctl->control(key);
}
