#include "ofMain.h"
#include "ofApp.h"

//========================================================================
int main(int argc, char **argv) 
{
	ofSetupOpenGL(1024,768,OF_WINDOW);			// <-------- setup the GL context

	// this kicks off the running of my app
	// can be OF_WINDOW or OF_FULLSCREEN
	const char *imgf = "";
	if (argc > 1) {
		imgf = argv[1];
	}

	// pass in width and height too:
	ofRunApp(new ofApp(imgf));

}
